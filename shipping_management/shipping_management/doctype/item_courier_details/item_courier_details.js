// Copyright (c) 2020, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Courier Details', {
	refresh(frm) {
	    var number_list = [];
var number_list_value= [];
    frappe.call({
      method: "frappe.client.get_list",
      args: {
        doctype: "Supplier",
        fields: ['supplier_name'],
        filters: {
          supplier_group: 'Transport Companies', 
	 },
        
      },
       callback: function(r) {
       	number_list = r.message;
       	// console.log(number_list)
       	for(var i=0;i<number_list.length;i++){
        var options = number_list[i];
        // console.log(options)
        // number_list_value= Array.from(Object.keys(options), k => options[k]);
        console.log(number_list_value)
// number_list_value=number_list_value.filter(Boolean);
// frm.set_df_property('courier_name', 'options', number_list_value);
}
}
    });
	    //=========================
	    var courier_names = ""
	    var strArr = []
	    var full_str = "\n"
	   frappe.db.get_list('Supplier',  {
    fields: ['supplier_name'],
    filters: {
        supplier_group : 'Transport Companies'
    }
}).then(records => {
    // console.log(records);
    for(var i=0;i<records.length;i++){
        // console.log(records[i].supplier_name)
        courier_names = records[i].supplier_name 
        strArr.push(courier_names)
        full_str=full_str.concat(courier_names)
        // console.log(full_str)
        // frm.set_df_property('courier_name', 'options', full_str);
    }
    console.log(strArr.map(String))
    full_str = strArr.join('\n')
    var full_str2 = toString(full_str).replace("**","$$")
    console.log(full_str)
    frm.set_df_property('courier_name', 'options', full_str);
})

	   // frm.set_df_property('courier_name', 'options', ["1","2","3","4","5"]);
        //===========================
	    $("[data-fieldname=shipping_address]" ).css("height", "220px");
	    $("[data-fieldname=shipping_address]" ).css("overflow", "overlay");
	// ============= get supplier whose customer group is transport ===============
    cur_frm.set_query('courier_name', function () {
    return {
        filters: {
            'supplier_group': 'Transport Companies'
        }
    }
});
	},
	courier_name:function(frm){
	    frappe.db.get_value("Supplier",{"supplier_name":frm.doc.courier_name},["gst_transporter_id"]).then((res1) => {
	        cur_frm.set_value("courier_gst_id",res1.message.gst_transporter_id);
	       // console.log(res1);
	    })
	}
})