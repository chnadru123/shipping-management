# -*- coding: utf-8 -*-
# Copyright (c) 2021, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document

class MonthlyAbsentForm(Document):
	def validate(self):
		for comp in self.employee_absent:
			if comp.overtime > 0:
				self.create_component(comp.employee,comp.overtime,"Over Time")
			if comp.holiday > 0:
				self.create_component(comp.employee,comp.holiday,"Incentive")
			if comp.bonus > 0:
				self.create_component(comp.employee,comp.bonus,"Bonus")
			if comp.advance > 0:
				self.create_component(comp.employee,comp.advance,"Advance Deduction")
			if comp.other > 0:
				self.create_component(comp.employee,comp.other,"Other Deduction")
			
	def fill_employee(self):
		self.set('employee_absent', [])
		
		employees = frappe.db.sql("""select distinct t1.name  as employee,t1.pf_account_number as pf_number from `tabEmployee` t1, `tabSalary Structure Assignment` t2 where t1.name = t2.employee and t2.docstatus = 1 order by pf_account_number""", as_dict=True)

		if not employees:
			frappe.throw(_("No employees for the mentioned criteria"))

		for d in employees:
			self.append('employee_absent', d)

	def create_component(self,employee,value,component):
		if value > 0:
			company = frappe.db.get_value('Employee', employee, 'company')
			additional_salary = frappe.new_doc('Additional Salary')
			additional_salary.employee = employee
			additional_salary.salary_component = component
			additional_salary.amount = value
			additional_salary.payroll_date = self.start_date
			additional_salary.company = company
			additional_salary.submit()
			frappe.msgprint("Salary Component created for "+employee)
			

