# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from erpnext.stock.utils import get_stock_balance

def execute(filters=None):
	columns, data = get_column(filters),get_data(filters)
	return columns, data

def get_data(filters):
	data = []
	bom_material = frappe.db.get_value("BOM",{"item":filters.get("item")},"name")
	if bom_material:
		bom_items = frappe.db.get_list("BOM Item",{"parent":bom_material},["item_code","item_name","qty"])
		for item in bom_items:
			# frappe.throw(str(item))
			act_qty = 0
			st_qty = 0
			if get_stock_balance(item.item_code,filters.get("warehouse")) - item.qty * float(filters.get("qty")) < 0:
				st_qty = get_stock_balance(item.item_code,filters.get("warehouse")) - item.qty * float(filters.get("qty"))
			po_qty = frappe.db.sql("""select pi.item_code,sum(pi.stock_qty - pi.received_qty) as act_qty from `tabPurchase Order Item` pi where pi.docstatus = 1 and pi.received_qty != pi.stock_qty and pi.item_code = %s group by pi.item_code""",(item.item_code),as_dict = 1)
			if len(po_qty) > 0:
				act_qty = po_qty[0].act_qty
			# frappe.msgprint(str(po_qty))
			data.append({
				"raw_material":item.item_code,
				"raw_material_name":item.item_name,
				"avl_qty":get_stock_balance(item.item_code,filters.get("warehouse")),
				"st_qty":st_qty,
				"po_qty":act_qty
			})
	return data

	

	


def get_column(filters):

	return[
		{
			"label": _("Raw Material"),
			"fieldname": "raw_material",
			"fieldtype": "Link",
			"options":"Item",
			"width": 150
		},
		{
			"label": _("Raw Material Name"),
			"fieldname": "raw_material_name",
			"fieldtype": "Data",
			"width": 250
		},
		{
			"label": _("Available Qty"),
			"fieldname": "avl_qty",
			"fieldtype": "float",
			"width": 100
		},
		{
			"label": _("Short Qty"),
			"fieldname": "st_qty",
			"fieldtype": "float",
			"width": 100
		},
		{
			"label": _("PO Qty"),
			"fieldname": "po_qty",
			"fieldtype": "float",
			"width": 100
		},

	]
