# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	validate_filters(filters)
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def validate_filters(filters):
	if filters.from_date > filters.to_date:
		frappe.throw(_("From Date must be before To Date"))

def get_data(filters):
	data = frappe.db.sql("""Select cs.billing_state as state,
	cs.customer_group,
	cs.territory,
	si.posting_date as invoice_date,
	 si.name as invoice_no,
	 si.customer as customer_name,
	st.item_code as item,
	st.item_name,
	st.item_brand as brand,
	 st.item_group as item_group,
	 st.qty as qty,
	 st.rate as rate,
	 st.amount,
	sp.sales_person 
	from 
	`tabSales Invoice` si inner join `tabSales Invoice Item` st on si.name = st.parent
	left join `tabSales Team` as sp on si.name = sp.parent
	left join `tabCustomer` as cs on si.customer = cs.name
	where st.item_group != "Shipping Item" and
	si.naming_series in ("TI-S-","TI-M-","CN-") and si.docstatus < 2 {conditions} """.format(conditions=get_conditions(filters)),filters,as_dict = 1)
	# frappe.throw(str(data))
	return data

def get_conditions(filters):
	conditions = []
	if filters.get("from_date") and filters.get("to_date"):
		conditions.append("si.posting_date between %(from_date)s and %(to_date)s")

	if filters.get("customer_group"):
		conditions.append(get_customer_group_condition(filters.get("customer_group")))

	if filters.get("customer"):
		conditions.append("si.customer = %(customer)s")

	if filters.get("item"):
		conditions.append("st.item_code = %(item)s")
	
	if filters.get("item_group"):
		conditions.append(get_item_group_condition(filters.get("item_group")))

	if filters.get("sales_person"):
		# frappe.msgprint("filter found")
		conditions.append(get_sales_person_condition(filters.get("sales_person")))

	if filters.get("territory"):
		conditions.append(get_territory_condition(filters.get("territory")))

	# frappe.throw(str("and {}".format(" and ".join(conditions)) if conditions else ""))
	return "and {}".format(" and ".join(conditions)) if conditions else ""

def get_item_group_condition(item_group):
	item_group_details = frappe.db.get_value("Item Group", item_group, ["lft", "rgt"], as_dict=1)
	if item_group_details:
		return "st.item_group in (select ig.name from `tabItem Group` ig \
			where ig.lft >= %s and ig.rgt <= %s and st.item_group = ig.name)"%(item_group_details.lft,
			item_group_details.rgt)

	return ''

def get_customer_group_condition(customer_group):
	customer_group_details = frappe.db.get_value("Customer Group", customer_group, ["lft", "rgt"], as_dict=1)
	if customer_group_details:
		return "cs.customer_group in (select ig.name from `tabCustomer Group` ig \
			where ig.lft >= %s and ig.rgt <= %s and cs.customer_group = ig.name)"%(customer_group_details.lft,
			customer_group_details.rgt)

	return ''

def get_territory_condition(territory):
	territory_details = frappe.db.get_value("Territory", territory, ["lft", "rgt"], as_dict=1)
	if territory_details:
		return "cs.territory in (select ig.name from `tabTerritory` ig \
			where ig.lft >= %s and ig.rgt <= %s and cs.territory = ig.name)"%(territory_details.lft,
			territory_details.rgt)

	return ''

def get_sales_person_condition(sales_person):
	sales_person_details = frappe.db.get_value("Sales Person", sales_person, ["lft", "rgt"], as_dict=1)
	if sales_person_details:
		return "sp.sales_person in (select ig.name from `tabSales Person` ig \
			where ig.lft >= %s and ig.rgt <= %s and sp.sales_person = ig.name)"%(sales_person_details.lft,
			sales_person_details.rgt)

	return ''

def get_columns(filters):
	columns = [
		
		{
			"label":_("Invoice Date"),
			"fieldname": "invoice_date",
			"fieldtype": "Date",
			"width": 90
		},
		{
			"label":_("Invoice No"),
			"fieldname": "invoice_no",
			"fieldtype": "Link",
			"options":"Sales Invoice",
			"width": 90
		},
		{
			"label":_("Sales Person"),
			"fieldname": "sales_person",
			"fieldtype": "Data",
			"width": 80
		},
		{
			"label":_("Customer Category"),
			"fieldname": "customer_group",
			"fieldtype": "Data",
			"width": 120
		},
		{
			"label":_("Customer Name"),
			"fieldname": "customer_name",
			"fieldtype": "Link",
			"options": "Customer",
			"width": 200
		},	
		{
			"label":_("State"),
			"fieldname": "state",
			"fieldtype": "Data",
			"width": 80
		},
		{
			"label":_("Territory"),
			"fieldname": "territory",
			"fieldtype": "Data",
			"width": 90
		},
		{
			"label":_("Brand"),
			"fieldname": "brand",
			"fieldtype": "Data",
			"width": 80
		},
		{
			"label":_("Item Group"),
			"fieldname": "item_group",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label":_("Item"),
			"fieldname": "item",
			"fieldtype": "Data",
			"width": 60
		},
		{
			"label":_("Item Name"),
			"fieldname": "item_name",
			"fieldtype": "Data",
			"width": 200
		},
		{
			"label":_("Qty"),
			"fieldname": "qty",
			"fieldtype": "Float",
			"width": 60
		},
		{
			"label":_("Rate"),
			"fieldname": "rate",
			"fieldtype": "Float",
			"width": 60
		},
		{
			"label":_("Amount"),
			"fieldname": "amount",
			"fieldtype": "Float",
			"width": 80
		}
		
		
	]

	return columns