# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
	validate_filters(filters)
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def validate_filters(filters):
	if filters.from_date > filters.to_date:
		frappe.throw(_("From Date must be before To Date"))

def get_data(filters):
	data = frappe.db.sql("""Select 
	so.name,
	so.workflow_state,
	so.sales_person,
	so.customer,
	so.transaction_date as date,
	so.rounded_total,
	so.paid_amount,
	so.payment_remarks,
	so.rejection_reason as reason,
	so.delivery_status,
	so.billing_status,
	so.status,
	sio.item_code,
	sio.qty,
	sio.delivered_qty,
	sio.rate,
	sio.amount from `tabSales Order` so inner join `tabSales Order Item` sio on so.name = sio.parent where so.transaction_date between %s and %s and so.workflow_state in ("Draft","Approved","Approval Level1","Approval Level2") and so.status in ("Draft","To Deliver and Bill") and sio.qty != sio.delivered_qty and sio.item_group != "Shipping Item" """,(filters.get("from_date"),filters.get("to_date")),as_dict = 1)
	# frappe.throw(str(data))
	return data



def get_columns(filters):
	columns = [
		
		{
			"label":_("ID"),
			"fieldname": "name",
			"fieldtype": "Link",
			"options":"Sales Order",
			"width": 90
		},
		{
			"label":_("Date"),
			"fieldname": "date",
			"fieldtype": "Date",
	
			"width": 90
		},
		{
			"label":_("Customer Name"),
			"fieldname": "customer",
			"fieldtype": "Data",
	
			"width": 90
		},
		
		{
			"label":_("Item Code"),
			"fieldname": "item_code",
			"fieldtype": "Data",
	
			"width": 90
		},
		{
			"label":_("Quantity"),
			"fieldname": "qty",
			"fieldtype": "Float",
	
			"width": 90
		},
		{
			"label":_(" Delivered Qty"),
			"fieldname": "delivered_qty",
			"fieldtype": "Float",
	
			"width": 90
		},
		{
			"label":_("Rate"),
			"fieldname": "rate",
			"fieldtype": "Float",
	
			"width": 90
		},
		{
			"label":_("Amount"),
			"fieldname": "amount",
			"fieldtype": "Float",
	
			"width": 90
		},
		# {
		# 	"label":_("Workflow State"),
		# 	"fieldname": "workflow_state",
		# 	"fieldtype": "Data",
		# 	"width": 90
		# },
		# {
		# 	"label":_("Delivery Status"),
		# 	"fieldname": "delivery_status",
		# 	"fieldtype": "Data",
		# 	"width": 90
		# },
		# {
		# 	"label":_("Billing Status"),
		# 	"fieldname": "billing_status",
		# 	"fieldtype": "Data",
		# 	"width": 90
		# },
		{
			"label":_("Status"),
			"fieldname": "status",
			"fieldtype": "Data",
			"width": 90
		},
		{
			"label":_("Sales Person"),
			"fieldname": "sales_person",
			"fieldtype": "Data",
			
			"width": 90
		},
		
		{
			"label":_("Rounded Total"),
			"fieldname": "rounded_total",
			"fieldtype": "Float",
	
			"width": 90
		},
		{
			"label":_("Paid Amount"),
			"fieldname": "paid_amount",
			"fieldtype": "Float",
	
			"width": 90
		},
		{
			"label":_("Payment Remarks"),
			"fieldname": "payment_remarks",
			"fieldtype": "data",
	
			"width": 90
		},
		{
			"label":_("Approval/Rejection Reason"),
			"fieldname": "reason",
			"fieldtype": "data",
	
			"width": 90
		}
		

		
		
		
	]

	return columns
