import frappe
from frappe import msgprint, _
from frappe.utils import add_days, cint, cstr, flt, getdate, rounded, date_diff, money_in_words
import re

def validate(self,method):
    incentive = 0
    for item in self.earnings:
        if item.salary_component in ["Over Time","Holiday Pay","Bonus","Incentive"]:
            incentive = incentive + item.amount
    if incentive:
        update_deduction(self,method,str(incentive))
    else:
        update_deduction(self,method,str(0))

    # if any(d.salary_component == 'Incentive' for d in self.earnings):
        
    #     res = next((sub for sub in self.earnings if sub.salary_component == "Incentive"), None)
    #     update_deduction(self,method,str(res.amount))
    #     # msgprint(str(res.amount))
    # else:
    #     pass
    #     # msgprint("Not Incentive")

    for item in self.deductions:
        emp_contri = frappe.db.get_value("Salary Component",item.salary_component,"employer_contribution")
        if emp_contri:
            # frappe.msgprint(str(item.salary_component))
            item.employer_contri = emp_contri
    tds_amount = get_tds(self.employee)
    if tds_amount:
        if not any(d.salary_component == 'TDS' for d in self.deductions):
            add_component_to_ss(self,"TDS",tds_amount)
    
    self.total_deduction = self.get_component_totals("deductions")
    set_gross_netpay(self)
    
    



def get_tds(employee):
    tds_amount = frappe.db.get_value("Employee",employee,"tds_amount")
    return tds_amount

def add_component_to_ss(self,component,amount,do_not_include_in_total = 0):
    row = self.append("deductions", {})
    row.salary_component = component
    row.amount = amount

def set_gross_netpay(self):
    # employer_contribuotion = 0
    
    # pass
    # leave_amount = 0
    # # for item in self.earnings:
    # #     if item.salary_component == "Leave Encashment":
    # #         leave_amount = leave_amount + item.amount
    # # self.custom_gross_pay = self.gross_pay - leave_amount

    employer_contri = 0
    for item in self.deductions:
        if item.employer_contri == 1:
            employer_contri = employer_contri + item.amount

    self.actual_deduction_custom = self.total_deduction - employer_contri
    self.custom_net_pay = self.gross_pay - self.actual_deduction_custom
    self.employer_contribution_custom = employer_contri

    # self.custom_net_pay = self.net_pay + employer_contri



def update_deduction(self,method,incentive_amt):

    data = self.get_data_for_eval()
    # frappe.throw(str(data))
    updated_data = update_data(data,incentive_amt)
    # frappe.msgprint(str(updated_data))
    # frappe.msgprint(str(self._salary_structure_doc.get("deductions")[0].salary_component))
    for struct_row in self._salary_structure_doc.get("deductions"):
        # frappe.msgprint(str(struct_row.salary_component))
        # frappe.msgprint(str(struct_row.formula))
        # frappe.msgprint(str(struct_row.condition))
        amount = eval_condition_and_formula_custom(self,struct_row, updated_data,incentive_amt)
        if amount and struct_row.statistical_component == 0:
            self.update_component_row(struct_row, round(amount), "deductions")
            frappe.msgprint("Deduction Component <b>{}</b> Update as per Incentive".format(struct_row.salary_component))
    # data = self.get_data_for_eval()
    # frappe.msgprint(str(data))
    # for item in self.deductions:
    #     frappe.msgprint(self.eval_condition_and_formula(item.salary_component,data))

def update_data(data,incentive_amt):
    # frappe.msgprint(str(data.base))
    # data.base = float(data.base) + float(incentive_amt)
    data.B = float(data.B) + float(incentive_amt)
    for item in data.earnings:
        if item.salary_component == "Basic":
            item.amount = float(item.amount) + float(incentive_amt)
            item.default_amount = float(item.default_amount) + float(incentive_amt)
            
    return data


def eval_condition_and_formula_custom(self, d, data,incentive_amt):
    # frappe.throw(str(data))
    # updated_data
    try:
        condition = d.condition.strip() if d.condition else None
        if condition:
            # frappe.msgprint(str(condition))
            # frappe.msgprint(str(condition))
            # frappe.msgprint(str(self.whitelisted_globals))
            # frappe.msgprint(str(data))
            # frappe.msgprint(str(frappe.safe_eval(condition, self.whitelisted_globals, data)))
            if not frappe.safe_eval(condition, self.whitelisted_globals, data):
                return None
        amount = d.amount
        # frappe.msgprint("Condition Amount "+str(amount))
        if d.amount_based_on_formula:
            formula = d.formula.strip() if d.formula else None
            if formula:
                # frappe.msgprint("Old Formula {}".format(str(formula)))  
                # updated_formula = formula    
                # flag = 0              
                
                # if re.search("^base*", formula):
                #     updated_formula = formula.replace('base','(base+{})'.format(incentive_amt))
                #     flag = 1
                # if re.search("B*", formula) and not flag:
                #     updated_formula = formula.replace('B','(B+{})'.format(incentive_amt))
                # frappe.msgprint("Formula {}".format(str(formula)))
                amount = flt(frappe.safe_eval(formula, self.whitelisted_globals, data), d.precision("amount"))
        if amount:
            data[d.abbr] = amount
        # frappe.msgprint("Formula Amount "+str(amount))

        return amount

    except NameError as err:
        frappe.throw(_("Name error: {0}".format(err)))
    except SyntaxError as err:
        frappe.throw(_("Syntax error in formula or condition: {0}".format(err)))
    except Exception as e:
        frappe.throw(_("Error in formula or condition: {0}".format(e)))
        raise