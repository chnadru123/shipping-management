import frappe
from erpnext.stock.utils import get_stock_balance

def validate(self,method):
    check_stock(self)

def check_stock(self):
    over_stock = 0
    out_of_stock = []
    for item in self.items:
        if item.s_warehouse:
            allow_negative = frappe.db.get_value("Warehouse",item.s_warehouse,"allow_negative_stock")
            if not allow_negative:
                warehouse_stock = get_stock_balance(item.item_code,item.s_warehouse)
                maintain_stock = frappe.db.get_value("Item",item.item_code,"is_stock_item")
                # frappe.msgprint(str(warehouse_stock)+" "+str(item.qty))
                if item.qty > warehouse_stock and maintain_stock:
                    over_stock = 1
                    out_of_stock.append({
                        "item_code":item.item_code,
                        "warehouse":item.s_warehouse,
                        "qty":item.qty,
                        "warehouse_qty":warehouse_stock,
                        "row":item.idx
                    })
    if over_stock == 1:
        # frappe.throw(str(out_of_stock))
        table = """<h3>Negative Stock Alert</h3><br><table class="table-bordered"><tr>
        <th>Row No.</th>
        <th>Item Code</th>
        <th>Warehouse</th>
        <th>Warehouse Qty</th>
        <th>Ordered Qty</th>
        <th>Short Qty</th>
        </tr>"""
        for item in out_of_stock:
            table = table + """<tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
            <td>{5}</td>
            </tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])))
        table = table + """</table>"""
        frappe.throw(table)

@frappe.whitelist()
def send_email_notification(stock_entry,out_going_entry):
    pretransfered = frappe.db.get_value("Stock Entry",out_going_entry,"per_transferred")
    if pretransfered < 100:
        stock_voucher = frappe.get_doc("Stock Entry",out_going_entry)
        stock_items = []
        for item in stock_voucher.items:
            if item.qty != item.transferred_qty:
                stock_items.append({
                    "item_code":item.item_code,
                    "item_name":item.item_name,
                    "tarnsfered":item.qty,
                    "received":item.transferred_qty
                })
                # frappe.msgprint(str(item.transferred_qty))
        # frappe.throw(str(stock_items))
        args = {
            "outward":out_going_entry,
            "inward":stock_entry,
            "stock_items":stock_items
        }
        user_list = frappe.get_list("User",[["Has Role","role","=","Stock Manager"],["enabled","=",1]],["name"])
        # frappe.msgprint(str(user_list))
        for user in user_list:
            frappe.msgprint(user["name"])
            frappe.sendmail(
                recipients=user["name"],
                subject="Stock Mismatch Alert",
                template="stock_transfer_alert",
                args=args)

