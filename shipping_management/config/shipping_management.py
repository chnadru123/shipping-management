from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Human Resourse Manegement"),
			"items": [
                {
					"type": "doctype",
					"name": "Employee",
					"onboard": 1,
				},
                {
					"type": "doctype",
					"name": "Salary Slip",
					"onboard": 1,
				},
				{
				"type": "doctype",
				"name": "Holiday List",
				"onboard": 1,
				},
				{
				"type": "doctype",
				"name": "Leave Application",
				"onboard": 1,
				}
				
			]
		}
    ]