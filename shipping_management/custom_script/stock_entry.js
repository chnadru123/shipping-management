frappe.ui.form.on('Stock Entry', {
    on_submit : function(frm) {        
        if(frm.doc.outgoing_stock_entry && frm.doc.stock_entry_type == "Receive at Warehouse"){
            frappe.call({
                "method":"shipping_management.shipping_management.stock_entry.send_email_notification",
                "args":{
                    "stock_entry":frm.doc.name,
                    "out_going_entry":frm.doc.outgoing_stock_entry
                }
            })
        }
    },
    stock_entry_type(frm){

	    if(frm.doc.stock_entry_type == "Material Issue"){
	        frm.set_value("naming_series","MI-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Receipt"){
	        frm.set_value("naming_series","MR-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Transfer"){
	        frm.set_value("naming_series","MT-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Transfer for Manufacture"){
	        frm.set_value("naming_series","MTM-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Consumption for Manufacture"){
	        frm.set_value("naming_series","MCM-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Manufacture"){
	        frm.set_value("naming_series","MF-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Send to Warehouse"){
	        frm.set_value("naming_series","SW-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Receive at Warehouse"){
	        frm.set_value("naming_series","RW-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "For Rejection"){
	        frm.set_value("naming_series","REJ-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Replacement"){
	        frm.set_value("naming_series","REP-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Repacking"){
	        frm.set_value("naming_series","RPK-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Stock JV"){
	        frm.set_value("naming_series","SJV-")
	        frm.refresh_field("naming_series")
	    }
	}


})