frappe.ui.form.on('Delivery Note', {
    refresh : function(frm, cdt, cdn) {
        // item_code is a field in grid whose fieldname is items.. refer Delivery Note
        frm.set_query("item_code", "items", function(doc) {
            
                return { "filters" : [['item_group','!=','Shipping Item'],['is_sales_item','=',1]]}
            
               
           
        });
        
        console.dir(cur_frm.fields_dict.items.grid.get_field("item_code"));
        },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    
    shipping_method:async function(frm){

        if(frm.doc.shipping_method)
        {let weight =await calculate_weight(frm.doc.total_net_weight,frm.doc.shipping_method)
        // console.log(weight)
        // Check if customer exist or not
        if (!frm.doc.customer){
            frm.set_value("shipping_method","")
            refresh_field("shipping_method");
        }

        // Check if Weight exist or not
        if (!frm.doc.total_net_weight){
            frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
            frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            
            
        }

        // update Shipping Item If already Present
        var found = 0
        for(var i = 0; i < frm.doc.items.length; i++){
            
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = weight
                cur_frm.script_manager.trigger("item_code",frm.doc.items[i]['doctype'] , frm.doc.items[i]['name']);

                found = 1
            }
        }


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            // console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    // console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Delivery Note Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    
                    newrow.qty = weight
                    cur_frm.script_manager.trigger("item_code", newrow.doctype, newrow.name);
                    
                    
                }
            })

        }}

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 

        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
                
            }
        }
        // console.log(shipping)
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        // console.log("here")
        
        frm.set_value("custom_discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        refresh_field("custom_discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        
        for(var i = 0; i < frm.doc.items.length; i++){
            
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
                // console.log(distributed_amount)
            }
        }

        
    }

    
    
})

frappe.ui.form.on('Delivery Note Item', {
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("inif   ")
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

function get_value (doctype,fieldname,filters) {
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                method:'frappe.client.get_value',
                args: {
                    doctype: doctype,
                    fieldname: fieldname,
                    filters:filters
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });
}

async function calculate_weight(current_weight,item){
    let minimum = await get_value("Item","minimum_weight",{"name":item})
    console.log(minimum.message.minimum_weight)
    if (Math.ceil(current_weight) >= minimum.message.minimum_weight){
        return Math.ceil(current_weight)
    }else{
        return minimum.message.minimum_weight
    }

}