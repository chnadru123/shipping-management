frappe.ui.form.on('Sales Order', {
    make_payment_c:function(frm){
            if(!frm.doc.payment_entry){
                frappe.confirm('Are you sure you want make payment?',
                    () => {
                        frappe.call({
                            "method":"shipping_management.shipping_management.sales_order.make_payment_entry",
                            "args":{
                                "self":frm.doc
                            },
                            "callback":function(res){
                                console.log(res)
                                frm.set_value("payment_entry",res.message.name)
                                frm.refresh_field("payment_entry")
                            }
                        })
                    }, () => {
                        // action to perform if No is selected
                    })
                
            }else{
                frappe.msgprint("Payment Entry Made Already!")
            }
            
        
    },
    refresh_outstanding:function(frm){
        frappe.call({
            "method":"shipping_management.shipping_management.sales_order.refresh_account_data",
            "args":{
                "self":frm.doc
            },
            "callback":function(res){
                console.log(res)
                frm.set_value("credit_limit",res.message.credit_limit)
                frm.set_value("current_outstanding",res.message.current_outstanding)
                frm.set_value("outstanding_limit",res.message.outstanding_limit)
                frm.set_value("overdue_amount",res.message.overdue_amount)
                frm.set_value("approved_sales_order",res.message.approved_sales_order)
                frm.set_value("current_ledger_balance",res.message.current_ledger_balance)
                frm.refresh_field("credit_limit","current_outstanding","outstanding_limit","approved_sales_order","current_ledger_balance")
            }
        })
    },
    onload:function(frm){
        if(frm.doc.name.search("New Sales Order") >= 0){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_sales_person",
                "args":{
                    "user":frappe.session.user
                },
                callback:function(res){
                    console.log(res)
                    if(res.message){
                        var newrow = frappe.model.add_child(frm.doc, "Sales Team", "sales_team");
                        newrow.sales_person = res.message;
                        newrow.allocated_percentage = 100;
                        cur_frm.script_manager.trigger("sales_person", newrow.doctype, newrow.name);
                        frm.refresh_field("sales_team");
    
                    }
                }
    
            })
        }
        
    },
    validate:function(frm){

        frm.set_value("sales_person",frm.doc.sales_team[0].sales_person)
	    frm.refresh_field("sales_person")
        frappe.db.get_value("Customer",frm.doc.customer,"accounting_issue").then((res)=>{
           console.log(res.message.accounting_issue)
           if(res.message.accounting_issue){
               frappe.msgprint("Customer is having Accounting Issue. Please contact Accounts Department to resolve the issue. You can make Sales Order but Invoice is blocked.")
           }
           
        })
        
       //  if(frm.doc.outstanding_limit){
       //      frappe.msgprint("Customer has old outstanding of Rs."+frm.doc.outstanding_limit+". Are you sure you want to continue")
       //  }
      
   },
    refresh :async function(frm, cdt, cdn) {
        console.log(frm.doc.customer_loyalty);
        // var loyalty_type = frm.doc.customer_loyalty;
        let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.customer,"customer_loyalty");
        if(loyalty_type_res.message){
            var loyalty_type = loyalty_type_res.message.customer_loyalty;
            let btn = document.createElement('span');
            if(loyalty_type == "New"){
                {
                    btn.innerText = 'New';
                    btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
                }
            } else if(loyalty_type == "Silver") {
                {
                    btn.innerText = 'Silver';
                    btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
                }
            } else if(loyalty_type == "Gold") {
                {
                    btn.innerText = 'Gold';
                    btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
                }

            } else if(loyalty_type == "Platinum")
                {
                    btn.innerText = 'Platinum';
                    btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
                }
                var tool_bar = $(".form-inner-toolbar")
                tool_bar.append(btn)
                console.log($(".form-inner-toolbar"))

        }
        

                
                
        //	btn.style = 'float:left';
            

        if(frm.doc.status == "Closed" && (frm.doc.per_delivered != 100 || frm.doc.per_billed != 100)){
            console.log("Changed Delivered and Billed Values")
            frappe.db.set_value("Sales Order",frm.doc.name,"per_delivered",100)
            frappe.db.set_value("Sales Order",frm.doc.name,"per_billed",100)
        }

        cur_frm.add_custom_button(__('Check Stock Details'), function() {
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.check_stock",
                "args":{
                    "order_doc":frm.doc,
                },
                
            })
        });
        // item_code is a field in grid whose fieldname is items.. refer Sales Order
        frm.set_query("item_code", "items", function(doc) {
            
                return { "filters" : [['item_group','!=','Shipping Item'],['is_sales_item','=',1]]}
            
               
           
        });
        
        console.dir(cur_frm.fields_dict.items.grid.get_field("item_code"));
        },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    
    // shipping_method:async function(frm){

    //     if(frm.doc.shipping_method)
    //     {let weight =await calculate_weight(frm.doc.total_net_weight,frm.doc.shipping_method)
    //     // console.log(weight)
    //     // Check if customer exist or not
    //     if (!frm.doc.customer){
    //         frm.set_value("shipping_method","")
    //         refresh_field("shipping_method");
    //     }

    //     // Check if Weight exist or not
    //     if (!frm.doc.total_net_weight){
    //         frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
    //         frm.set_value("shipping_method","")
    //         refresh_field("shipping_method");
            
            
    //     }

    //     // update Shipping Item If already Present
    //     var found = 0
    //     for(var i = 0; i < frm.doc.items.length; i++){
            
    //         if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                
    //             frm.doc.items[i]['item_code'] = frm.doc.shipping_method
    //             frm.doc.items[i]['qty'] = weight
    //             cur_frm.script_manager.trigger("item_code",frm.doc.items[i]['doctype'] , frm.doc.items[i]['name']);

    //             found = 1
    //         }
    //     }


    //     // If not Shipping Item then add it.
    //     if (frm.doc.shipping_method && !found){
    //         // console.log("add item")
    //         frappe.call({
    //             method:"frappe.client.get",
    //             args:{
    //                 "doctype":"Item",
    //                 "name":frm.doc.shipping_method
    //             },
    //             callback:function(res){

    //                 // console.log(res.message)
    //                 var newrow = frappe.model.add_child(frm.doc, "Sales Order Item", "items");
    //                 newrow.item_code = res.message['name'];
    //                 newrow.item_name = res.message['item_name'];
                    
    //                 newrow.qty = weight
    //                 cur_frm.script_manager.trigger("item_code", newrow.doctype, newrow.name);
                    
                    
    //             }
    //         })

    //     }}

    //     // If shipping Method Field is Empty then remove it from table
    //     if (!frm.doc.shipping_method){
    //         for(var i = 0; i < frm.doc.items.length; i++){
    //             // console.log(frm.doc.items[i]['item_group'])
    //             if(frm.doc.items[i]['item_group'] == "Shipping Item"){
    //                 // console.log("inif   ")
    //                 frm.doc.items.pop(frm.doc.items[i])
    //                 refresh_field("items");
    //             }
    //         }
    //     } 

    //     var shipping = 0
    //     for(var i = 0; i < frm.doc.items.length; i++){
    //         // console.log(frm.doc.items[i]['item_group'])
    //         if(frm.doc.items[i]['item_group'] == "Shipping Item"){
    //             shipping = shipping + flt(frm.doc.items[i]['amount'])
                
    //         }
    //     }
    //     // console.log(shipping)
    //     frm.set_value("shipping_charges",shipping)
    //     frm.set_value("item_total",flt(frm.doc.total - shipping))
    //     refresh_field("shipping_charges","item_total");
        
        

    // },
    shipping_method:async function(frm){
       
        if(frm.doc.shipping_method)
        {    
            // Check if shipping rule exist
            if(!frm.doc.shipping_rule_custom){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
                frappe.throw("Please Select Shipping Rule First!")
            }
    
            // Check if customer exist or not
            if (!frm.doc.customer){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
            }

            // Check if Weight exist or not
            if (!frm.doc.total_net_weight){
                frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");            
            }
        
        let shipping_amount =await calculate_shipping_amount(frm.doc)
        var found = 0
        // update Shipping Item If already Present
        
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("update item")
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = 1
                // cur_frm.script_manager.trigger("item_code",frm.doc.items[i]['doctype'] , frm.doc.items[i]['name']);
                frm.doc.items[i]['rate'] = shipping_amount.message
                
                found = 1
                refresh_field("items");
            }
        }       


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Sales Order Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    newrow.item_group = res.message['item_group'];
                    newrow.description = res.message['description'];
                    newrow.stock_uom = res.message['stock_uom'];
                    newrow.uom = res.message['stock_uom'];
                    newrow.income_account = res.message['item_defaults'][0]['income_account'];
                    newrow.rate = shipping_amount.message
                    newrow.qty = 1
                    cur_frm.script_manager.trigger("rate", newrow.doctype, newrow.name);       
                }
            })

        }
    }

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 

        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
                
            }
        }
        // console.log(shipping)
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        // console.log("here")
        if(frm.doc.item_total){
            frm.set_value("custom_discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        }else{
            frm.set_value("custom_discount_amount",flt(frm.doc.total*frm.doc.custom_discount_percentage)/100)
        }
        
        refresh_field("custom_discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        
        for(var i = 0; i < frm.doc.items.length; i++){
            
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
                // console.log(distributed_amount)
            }
        }

        
    }

    
    
})

cur_frm.cscript.rate = function(doc,cdt,cdn){
    //  console.log("rate change")
    update_item_total(doc,cdt,cdn)
    }
    // cur_frm.cscript.qty = function(doc,cdt,cdn){
    // update_item_total(doc,cdt,cdn)
    // }
function update_item_total(doc,cdt,cdn){
    var shipping = 0
    for(var i = 0; i < doc.items.length; i++){
        // console.log(doc.items[i]['item_group'])
        if(doc.items[i]['item_group'] == "Shipping Item"){
            shipping = shipping + flt(doc.items[i]['amount'])
            
        }
    }
    // console.log(shipping)
    cur_frm.set_value("shipping_charges",shipping)
    cur_frm.set_value("item_total",flt(doc.total - shipping))
    refresh_field("shipping_charges","item_total");
}

frappe.ui.form.on('Sales Order Item', {
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("inif   ")
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

function get_value (doctype,fieldname,filters) {
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                method:'frappe.client.get_value',
                args: {
                    doctype: doctype,
                    fieldname: fieldname,
                    filters:filters
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });
}

async function calculate_weight(current_weight,item){
    let minimum = await get_value("Item","minimum_weight",{"name":item})
    console.log(minimum.message.minimum_weight)
    if (Math.ceil(current_weight) >= minimum.message.minimum_weight){
        return Math.ceil(current_weight)
    }else{
        return minimum.message.minimum_weight
    }

}

async function calculate_shipping_amount(current_doc){
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                method:'shipping_management.shipping_management.sales_invoice.apply_shipping_rule_custom',
                args: {
                    curr_doc: current_doc,
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });
    // let minimum = await get_value("Item","minimum_weight",{"name":item})
    // console.log(minimum.message.minimum_weight)
    // if (Math.ceil(current_weight) >= minimum.message.minimum_weight){
    //     return Math.ceil(current_weight)
    // }else{
    //     return minimum.message.minimum_weight
    // }

}