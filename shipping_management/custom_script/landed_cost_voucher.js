frappe.ui.form.on('Landed Cost Voucher', {
	
})

$.extend(cur_frm.cscript, {
	is_domestic_invoice:(frm,cdt,cdn) => {
		var df = frappe.meta.get_docfield("Landed Cost Item","custom_duty", cur_frm.doc.name);
		var df1 = frappe.meta.get_docfield("Landed Cost Item","custom_duty_amount", cur_frm.doc.name);
		var df2 = frappe.meta.get_docfield("Landed Cost Item","assessable_value", cur_frm.doc.name);
		if(frm.is_domestic_invoice){
			df.read_only = 1
			df1.read_only = 1
			df2.read_only = 1
			refresh_field("items")
			cur_frm.refresh_field("distribute_charges_bases_on")
		}else{
			df.read_only = 0
			df1.read_only = 0
			df2.read_only = 0
			refresh_field("items")
			refresh_field("distribute_charges_bases_on")
		}
		
		
	},
	custom_duty:(frm,cdt,cdn) => {
		var row = locals[cdt][cdn]
		if(row.custom_duty){
			var rate = row.custom_duty
			
			var custom_amout = (row.assessable_value * rate)/100
			
			row.custom_duty_amount = custom_amout
			row.applicable_charges = custom_amout
			refresh_field("items")
		}
		// if(row.igst_rate){
		// 	cur_frm.script_manager.trigger("igst_rate", row.doctype, row.name);
		// 	row.applicable_charges = custom_amout + row.igst_amount
		// 	count_total_cost(frm, 'igst_amount', 'total_igst_amount')
		// }
		count_total_cost(frm, 'custom_duty_amount', 'total_custom_duty_amount')
	},

	// igst_rate:(frm,cdt,cdn) => {
	// 	var row = locals[cdt][cdn]
	// 	if(row.igst_rate){
	// 		var rate = row.igst_rate
			
	// 		var igst_amount = ((row.amount+row.custom_duty_amount) * rate)/100
			
	// 		row.igst_amount = igst_amount
	// 		row.applicable_charges = row.custom_duty_amount + row.igst_amount
	// 		refresh_field("items")
	// 	}

	// 	count_total_cost(frm, 'igst_amount', 'total_igst_amount')
	// },

	add_charges:async function(frm,cdt,cdn){
		let field = await get_accounts()
		
		if(field.message.custom_duty_account && field.message.igst_account){
			var data = [{acc:field.message.custom_duty_account,amt:frm.total_custom_duty_amount}]
			frm.taxes = []
			data.forEach(element => {
				var newrow = frappe.model.add_child(frm, "Landed Cost Taxes and Charges", "taxes");
				console.log(newrow)
                    newrow.expense_account = element.acc
					newrow.description = "Additional Charges"
					newrow.amount = element.amt
                    
                    refresh_field("taxes");
                    cur_frm.script_manager.trigger("expense_account", newrow.doctype, newrow.name)

			});

			cur_frm.set_value('total_taxes_and_charges',parseFloat(frm.total_igst_amount+frm.total_custom_duty_amount))
			refresh_field("total_taxes_and_charges")
		}else{
			frappe.throw("Please Select Accounts in Custom Buying Settings")
		}		
	}
});


function count_total_cost(frm,amonut_field,total_field){
	
	var total = 0
			frm.items.forEach(element => {
				
				total = total + parseFloat(element[amonut_field])
			});
			cur_frm.set_value(total_field,parseFloat(total))
			refresh_field(total_field)
	}

function get_accounts(){
	return new Promise(function (resolve, reject) {
		try {
			frappe.call({
				method: 'frappe.client.get',
				args: {
					doctype: "Custom Buying Setting",
				},
				callback: resolve
			});
		} catch (e) { reject(e); }
	});
}

