frappe.ui.form.on('Quotation', {
    validate:function(frm){
        if(frm.doc.quotation_to == "Customer"){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_customer_outstanding",
                "args":{
                    "customer":frm.doc.party_name,
                    "company":frm.doc.company
                },
                "callback":function(res){
                    console.log(res)
                    if(res.message > 0){
                        frappe.confirm(
                            'Customer has Outstanding of '+res.message+'. Do you want to Continue?',
                            function(){
                                window.close();
                            },
                            function(){
                                frm.set_value("total_with_outstanding_c",frm.doc.total + parseFloat(res.message))
                                frm.refresh_field("total_with_outstanding_c")
                            }
                        )
                        
                    }
                }
            })

        }
       
    },
    refresh : async function(frm, cdt, cdn) {
        
        // console.log(frm.doc.customer_loyalty);
    // var loyalty_type = frm.doc.customer_loyalty;
        let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.party_name,"customer_loyalty");
        var loyalty_type = loyalty_type_res.message.customer_loyalty;
        // console.log(loyalty_type_res)
        let btn = document.createElement('span');
    if(loyalty_type == "New"){
        {
            btn.innerText = 'New';
            btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
        }
    } else if(loyalty_type == "Silver") {
        {
            btn.innerText = 'Silver';
            btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
        }
    } else if(loyalty_type == "Gold") {
        {
            btn.innerText = 'Gold';
            btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
        }

    } else if(loyalty_type == "Platinum")
        {
            btn.innerText = 'Platinum';
            btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
        }

                
                
        //	btn.style = 'float:left';
            var tool_bar = $(".form-inner-toolbar")
            tool_bar.append(btn)
        // 	console.log($(".form-inner-toolbar"))




        // item_code is a field in grid whose fieldname is items.. refer Quotation
        frm.set_query("item_code", "items", function(doc) {
            
                return { "filters" : [['item_group','!=','Shipping Item'],['is_sales_item','=',1]]}
            
               
           
        }); 
        
        console.dir(cur_frm.fields_dict.items.grid.get_field("item_code"));
        },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    
    shipping_method:async function(frm){
        console.log("here")
       
        if(frm.doc.shipping_method)
        {    
            // Check if shipping rule exist
            if(!frm.doc.shipping_rule_custom){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
                frappe.throw("Please Select Shipping Rule First!")
            }
    
            // Check if customer exist or not
            if (!frm.doc.party_name){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
            }

            // Check if Weight exist or not
            if (!frm.doc.total_net_weight){
                frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");            
            }
        
        let shipping_amount =await calculate_shipping_amount(frm.doc)
        var found = 0
        // update Shipping Item If already Present
        
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("update item")
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = 1
                // cur_frm.script_manager.trigger("item_code",frm.doc.items[i]['doctype'] , frm.doc.items[i]['name']);
                frm.doc.items[i]['rate'] = shipping_amount.message
                
                found = 1
                refresh_field("items");
            }
        }       


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Quotation Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    newrow.item_group = res.message['item_group'];
                    newrow.description = res.message['description'];
                    newrow.stock_uom = res.message['stock_uom'];
                    newrow.uom = res.message['stock_uom'];
                    newrow.income_account = res.message['item_defaults'][0]['income_account'];
                    newrow.rate = shipping_amount.message
                    newrow.qty = 1
                    cur_frm.script_manager.trigger("rate", newrow.doctype, newrow.name);       
                }
            })

        }
    }

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 

        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
                
            }
        }
        // console.log(shipping)
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        // console.log("here")
        if(frm.doc.item_total){
            frm.set_value("custom_discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        }else{
            frm.set_value("custom_discount_amount",flt(frm.doc.total*frm.doc.custom_discount_percentage)/100)
        }
        
        refresh_field("custom_discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        // console.log("custo_d_a")
        for(var i = 0; i < frm.doc.items.length; i++){
            
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
                // console.log(distributed_amount)
            }
        }

        
    },
   
    
})

cur_frm.cscript.rate = function(doc,cdt,cdn){
    //  console.log("rate change")
    update_item_total(doc,cdt,cdn)
    }
    // cur_frm.cscript.qty = function(doc,cdt,cdn){
    // update_item_total(doc,cdt,cdn)
    // }
function update_item_total(doc,cdt,cdn){
    var shipping = 0
    for(var i = 0; i < doc.items.length; i++){
        // console.log(doc.items[i]['item_group'])
        if(doc.items[i]['item_group'] == "Shipping Item"){
            shipping = shipping + flt(doc.items[i]['amount'])
            
        }
    }
    // console.log(shipping)
    cur_frm.set_value("shipping_charges",shipping)
    cur_frm.set_value("item_total",flt(doc.total - shipping))
    refresh_field("shipping_charges","item_total");
}

frappe.ui.form.on('Quotation Item', {
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("inif   ")
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

function get_value (doctype,fieldname,filters) {
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                method:'frappe.client.get_value',
                args: {
                    doctype: doctype,
                    fieldname: fieldname,
                    filters:filters
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });
}

async function calculate_weight(current_weight,item){
    let minimum = await get_value("Item","minimum_weight",{"name":item})
    console.log(minimum.message.minimum_weight)
    if (Math.ceil(current_weight) >= minimum.message.minimum_weight){
        return Math.ceil(current_weight)
    }else{
        return minimum.message.minimum_weight
    }

}

async function calculate_shipping_amount(current_doc){
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                method:'shipping_management.shipping_management.sales_invoice.apply_shipping_rule_custom',
                args: {
                    curr_doc: current_doc,
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });
    // let minimum = await get_value("Item","minimum_weight",{"name":item})
    // console.log(minimum.message.minimum_weight)
    // if (Math.ceil(current_weight) >= minimum.message.minimum_weight){
    //     return Math.ceil(current_weight)
    // }else{
    //     return minimum.message.minimum_weight
    // }

}